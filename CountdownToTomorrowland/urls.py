"""CountdownToTomorrowland URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from travelmap.views import ItineraryDelete

try:
    from django.conf.urls import patterns, url
except ImportError:
    # Django < 1.4
    from django.conf.urls.defaults import patterns, url
from social.utils import setting_name
extra = getattr(settings, setting_name('TRAILING_SLASH'), True) and '/' or ''

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'travelmap.views.map', name="map_detail"),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^logout/$', 'countdown.views.logout', name="logout"),
    url(r'^new_itinerary/$', 'travelmap.views.new_itinerary', name="new_itinerary"),
    url(r'^edit_itinerary/(?P<username>[-\w]+)/$', 'travelmap.views.edit_itinerary', name="edit_itinerary"),
    url(r'^itinerary/(?P<username>[-\w]+)/$', 'travelmap.views.itinerary_detail', name="itinerary_detail"),
    url(r'^itinerary/(?P<pk>\d+)/delete/$', ItineraryDelete.as_view(), name="delete_itinerary"),
    url(r'^get_users/$', 'travelmap.views.get_users', name="get_users"),
    url(r'^about/$', 'countdown.views.about', name="about"),
    url(r'^feedback/$', 'countdown.views.feedback', name="feedback")



]
