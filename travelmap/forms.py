from django import forms
from travelmap.models import Itinerary
from django.forms import widgets
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, ButtonHolder, Field, HTML
from crispy_forms.bootstrap import InlineField, InlineCheckboxes, FormActions

# allows a user to create a Tomorrowland Itinerary
class ItineraryForm(forms.ModelForm):
    class Meta:
        model = Itinerary
        fields = ['departure_city',
                  'arrival_city',
                  'attending_fri',
                  'attending_sat',
                  'attending_sun',
                  'global_journey',
                  'global_journey_type',
                  'hotel_package',
                  'hotel',
                  'dreamville_package',
                  'dreamville',
                  'comments',
                  'arrival_lat',
                  'arrival_lng',
                  'departure_lat',
                  'departure_lng'
        ]

        exclude = ['user',
                   'updated'
        ]

        widgets = {
            'arrival_lat': forms.HiddenInput(),
            'arrival_lng': forms.HiddenInput(),
            'departure_lat': forms.HiddenInput(),
            'departure_lng': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super(ItineraryForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(

            Fieldset(
                'Departure and Arrival Info',
                'departure_city',
                'arrival_city'
            ),
            Fieldset(
                'Days Attending',
                InlineField(
                    'attending_fri',
                    'attending_sat',
                    'attending_sun'
                )
            ),
            Fieldset(
                'Accommodations Info',
                'global_journey',
                'global_journey_type',
                'dreamville_package',
                'dreamville',
                'hotel_package',
                'hotel',
            ),
            Field('comments', placeholder='Add any additional comments you would like '
                                          'to share with other Redditors about your Tomorrowland travel plans.'),
            'arrival_lat',
            'arrival_lng',
            'departure_lat',
            'departure_lng',


            FormActions(
                Submit('save', 'Save Itinerary', css_class='btn btn-tomorrowland')
            )


        )

    def clean(self):
        cleaned_data = super(ItineraryForm, self).clean()
        departure_lat = cleaned_data.get("departure_lat")
        departure_lng = cleaned_data.get("departure_lng")
        arrival_lat = cleaned_data.get("arrival_lat")
        arrival_lng = cleaned_data.get("arrival_lng")

        if departure_lat == 91 or departure_lng == 181:
            self.add_error('departure_city', "The Departure City you entered was not a valid location")

        if arrival_lat == 91 or arrival_lng == 181:
            self.add_error('arrival_city', "The Arrival City you entered was not a valid location")

