from django.shortcuts import render, get_object_or_404
from travelmap.forms import ItineraryForm
from travelmap.models import Itinerary
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.contrib import messages
from django.views.generic import CreateView, DeleteView
import datetime
import json

def itinerary_detail(request, username):
    itinerary = get_object_or_404(Itinerary, user__username=username)
    context = {'itinerary': itinerary}

    return render(request, 'travelmap/itinerary_detail.html', context)

def get_users(request):
    if request.is_ajax():
        data = serializers.serialize("json",
                                  Itinerary.objects.all(),
                                  fields=('user',
                                          'departure_city',
                                          'arrival_city',
                                          'attending_fri',
                                          'attending_sat',
                                          'attending_sun',
                                          'global_journey',
                                          'global_journey_type',
                                          'hotel_package',
                                          'hotel',
                                          'dreamville_package',
                                          'dreamville',
                                          'comments',
                                          'arrival_lat',
                                          'arrival_lng',
                                          'departure_lat',
                                          'departure_lng'
                                  ),
                                  use_natural_foreign_keys=True)

        json_data = json.dumps(data)

        return HttpResponse(data, content_type='application/json')
    else:
        raise Http404
def map(request):
    users = serializers.serialize("json",
                                  Itinerary.objects.all(),
                                  fields=('user',
                                          'departure_city',
                                          'arrival_city',
                                          'attending_fri',
                                          'attending_sat',
                                          'attending_sun',
                                          'global_journey',
                                          'global_journey_type',
                                          'hotel_package',
                                          'hotel',
                                          'dreamville_package',
                                          'dreamville',
                                          'comments',
                                          'arrival_lat',
                                          'arrival_lng',
                                          'departure_lat',
                                          'departure_lng'
                                  ),
                                  use_natural_foreign_keys=True)
    users = json.dumps(users)

    context = {'users': users}
    return render(request, 'travelmap/map.html', context)


@login_required()
def new_itinerary(request):
    action = 'new_itinerary/'

    if Itinerary.objects.filter(user=request.user):
        url = reverse('edit_itinerary', kwargs={'username': request.user.username})
        return HttpResponseRedirect(url)

    if request.method == 'POST':
        form = ItineraryForm(request.POST)
        print "POST"

        # validate form
        if form.is_valid():
            itinerary = form.save(commit=False)
            itinerary.user = request.user
            itinerary.created = datetime.datetime.today()
            itinerary.save()
            messages.success(request, "Itinerary was successfully added to the map!")
            return HttpResponseRedirect(reverse('edit_itinerary', kwargs={'username': request.user.username}))

        else:
            messages.error(request, "Something went wrong. Check your Departure and Arrival cities and try again")
    else:
        # display form
        form = ItineraryForm()
        print "GET"

    # Bad form or no form supplied...
    # Render the form with error messages
    return render(request, 'travelmap/itinerary.html', {'form': form, 'action': action})

@login_required()
def edit_itinerary(request, username):
    user = request.user
    action = "edit_itinerary/%s/" % (username,)
    edit = True

    # Make sure user is editing his own itinerary!
    if user.username != username:
        raise Http404

    itinerary = get_object_or_404(Itinerary, user__username=username)

    if request.method == 'POST':
        form = ItineraryForm(request.POST, instance=itinerary)

        if form.is_valid():
            form.save()
            messages.success(request, "Itinerary successfully updated!")
            return HttpResponseRedirect(reverse('itinerary_detail', kwargs={'username': user.username}))
        else:
            messages.error(request, "Something went wrong. Make sure your Departure/Arrival locations are "
                                    "valid and try again.")

    else:
        # request was not POST, so display the form instance
        form = ItineraryForm(instance=itinerary)

    return render(request, 'travelmap/itinerary.html', {'form': form, 'action': action, 'edit': edit})


class ItineraryDelete(DeleteView):
    model = Itinerary
    success_url = reverse_lazy('map_detail')

    def get_object(self, queryset=None):
        # Hook to ensure object is owned by request.user
        obj = super(ItineraryDelete, self).get_object()
        if not obj.user == self.request.user:
            raise Http404

        return obj







