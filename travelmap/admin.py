from django.contrib import admin
from travelmap.models import Itinerary


admin.site.register(Itinerary)
