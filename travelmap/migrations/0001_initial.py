# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Itinerary',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('departure_city', models.CharField(max_length=255)),
                ('arrival_city', models.CharField(max_length=255)),
                ('arrival_lat', models.FloatField()),
                ('departure_lat', models.FloatField()),
                ('arrival_lng', models.FloatField()),
                ('departure_lng', models.FloatField()),
                ('attending_fri', models.BooleanField(default=True, verbose_name=b'Friday, July 24')),
                ('attending_sat', models.BooleanField(default=True, verbose_name=b'Saturday, July 25')),
                ('attending_sun', models.BooleanField(default=True, verbose_name=b'Sunday, July 25')),
                ('global_journey', models.BooleanField(default=False)),
                ('global_journey_type', models.CharField(blank=True, max_length=255, choices=[(b'Flight', b'Flight Package'), (b'Train', b'Train Package'), (b'Hotel', b'Hotel Package'), (b'Day', b'Day of Madness'), (b'Discover', b'Discover Europe')])),
                ('hotel_package', models.BooleanField(default=False)),
                ('hotel', models.CharField(blank=True, max_length=255, choices=[(b'Bedford', b'Bedford'), (b'Best Western County House', b'Best Western County House'), (b'Bloom!', b'Bloom!'), (b'Colonies Hotel', b'Colonies Hotel'), (b'Courtyard by Marriott', b'Courtyard by Marriott'), (b'Gresham Belson', b'Gresham Belson'), (b'Holiday Inn Airport', b'Holiday Inn Airport'), (b'Ibis Styles Louise', b'Ibis Styles Louise'), (b'Manos Premier', b'Manos Premier'), (b"Martin's EU", b"Martin's EU"), (b'Meininger', b'Meininger'), (b'Metropole', b'Metropole'), (b'Pentahotel Brussels Airport', b'Pentahotel Brussels Airport'), (b'Pentahotel Brussels City Center', b'Pentahotel Brussels City Center'), (b'Radisson Blu le Royal', b'Radisson Blu le Royal'), (b'Ramada Brussels Woluwe', b'Ramada Brussels Woluwe'), (b'Sheraton Brussels City', b'Sheraton Brussels City'), (b'Siru Hotel', b'Siru Hotel'), (b'Sofitel Louise', b'Sofitel Louise'), (b'Stanhope', b'Stanhope'), (b'The Hotel', b'The Hotel'), (b'Van Der Valk Airport', b'Van Der Valk Airport'), (b'Warwick Barsey', b'Warwick Barsey')])),
                ('dreamville_package', models.BooleanField(default=False)),
                ('dreamville', models.CharField(blank=True, max_length=255, choices=[(b'Magnificent Greens', b'Magnificent Greens'), (b'Friendship Garden', b'Friendship Garden'), (b'Fabulous Friendship Garden', b'Fabulous Friendship Garden'), (b'Easy Tent', b'Easy Tent'), (b'Fabulous Easy Tent', b'Fabulous Easy Tent'), (b'Relax Room', b'Relax Room'), (b'DreamLodge', b'DreamLodge'), (b'Fabulous DreamLodge', b'Fabulous DreamLodge'), (b'DreamVille Cabana', b'DreamVille Cabana'), (b'Exclusive Mansion', b'Exclusive Mansion')])),
                ('comments', models.TextField(blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
