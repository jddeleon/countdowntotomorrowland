# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travelmap', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='itinerary',
            name='global_journey_type',
            field=models.CharField(blank=True, max_length=255, choices=[(b'Flight Package', b'Flight Package'), (b'Train Package', b'Train Package'), (b'Hotel Package', b'Hotel Package'), (b'Day of Madness', b'Day of Madness'), (b'Discover Europe', b'Discover Europe')]),
        ),
    ]
