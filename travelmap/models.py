from django.db import models
from django.contrib.auth.models import User
import datetime

class Itinerary(models.Model):
    GLOBAL_JOURNEY_PACKAGE_TYPES = (
        ('Flight Package', 'Flight Package'),
        ('Train Package', 'Train Package'),
        ('Hotel Package', 'Hotel Package'),
        ('Day of Madness', 'Day of Madness'),
        ('Discover Europe', 'Discover Europe'),
    )

    HOTELS = (
        ('Bedford','Bedford'),
        ('Best Western County House','Best Western County House'),
        ('Bloom!','Bloom!'),
        ('Colonies Hotel','Colonies Hotel'),
        ('Courtyard by Marriott','Courtyard by Marriott'),
        ('Gresham Belson','Gresham Belson'),
        ('Holiday Inn Airport','Holiday Inn Airport'),
        ('Ibis Styles Louise','Ibis Styles Louise'),
        ('Manos Premier','Manos Premier'),
        ('Martin\'s EU','Martin\'s EU'),
        ('Meininger','Meininger'),
        ('Metropole','Metropole'),
        ('Pentahotel Brussels Airport','Pentahotel Brussels Airport'),
        ('Pentahotel Brussels City Center','Pentahotel Brussels City Center'),
        ('Radisson Blu le Royal','Radisson Blu le Royal'),
        ('Ramada Brussels Woluwe','Ramada Brussels Woluwe'),
        ('Sheraton Brussels City','Sheraton Brussels City'),
        ('Siru Hotel','Siru Hotel'),
        ('Sofitel Louise','Sofitel Louise'),
        ('Stanhope','Stanhope'),
        ('The Hotel','The Hotel'),
        ('Van Der Valk Airport','Van Der Valk Airport'),
        ('Warwick Barsey','Warwick Barsey'),
    )

    DREAMVILLE_TYPES = (
        ('Magnificent Greens','Magnificent Greens'),
        ('Friendship Garden','Friendship Garden'),
        ('Fabulous Friendship Garden','Fabulous Friendship Garden'),
        ('Easy Tent','Easy Tent'),
        ('Fabulous Easy Tent','Fabulous Easy Tent'),
        ('Relax Room','Relax Room'),
        ('DreamLodge','DreamLodge'),
        ('Fabulous DreamLodge','Fabulous DreamLodge'),
        ('DreamVille Cabana','DreamVille Cabana'),
        ('Exclusive Mansion','Exclusive Mansion'),
    )

    user = models.OneToOneField(User)
    departure_city = models.CharField(max_length=255, blank=False)
    arrival_city = models.CharField(max_length=255, blank=False)
    arrival_lat = models.FloatField()
    departure_lat = models.FloatField()
    arrival_lng = models.FloatField()
    departure_lng = models.FloatField()
    attending_fri = models.BooleanField(default=True, verbose_name='Friday, July 24')
    attending_sat = models.BooleanField(default=True, verbose_name='Saturday, July 25')
    attending_sun = models.BooleanField(default=True, verbose_name='Sunday, July 25')
    global_journey = models.BooleanField(default=False)
    global_journey_type = models.CharField(max_length=255, choices=GLOBAL_JOURNEY_PACKAGE_TYPES, blank=True)
    hotel_package = models.BooleanField(default=False)
    hotel = models.CharField(max_length=255, choices=HOTELS, blank=True)
    dreamville_package = models.BooleanField(default=False)
    dreamville = models.CharField(max_length=255, choices=DREAMVILLE_TYPES, blank=True)
    comments = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return str(self.user)

    # def save(self, *args, **kwargs):
    #     if not self.id:
    #         self.created = datetime.datetime.today()
    #
    #     self.updated = datetime.datetime.today()
    #     return super(Itinerary, self).save(*args, **kwargs)




