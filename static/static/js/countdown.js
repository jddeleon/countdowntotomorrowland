$(function() {

    $('#toggle').on('click', function(e) {
        e.preventDefault();

        if ($('#countdown').is(":hidden")) {
            $(this).html("Hide Countdown");
        } else {
            $(this).html("Show Countdown");
        }

        //$('#map-canvas').toggle();
        $('#countdown').fadeToggle('slow');




    });


    $("#countdown").TimeCircles({
        count_past_zero: false,
        total_duration: "Months",
        animation: "Ticks",
        bg_width: .5,
        fg_width: 0.04,

        time: {
            Days: { color: "#BB4478" },
            Hours: { color: "#BB4478" },
            Minutes: { color: "#BB4478" },
            Seconds: { color: "#BB4478" }

        }
    });

    $('#countdown').hide();

});