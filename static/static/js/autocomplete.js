var autocompleteArray = [];
var arrivalLat = $('#id_arrival_lat');
var arrivalLng = $('#id_arrival_lng');
var departureLat = $('#id_departure_lat');
var departureLng = $('#id_departure_lng');

function initializeAutocomplete() {
    for (var i=0; i < arguments.length; i++) {

        // Set up Autocomplete for Arrival and Departure city
        // Create the autocomplete object and associate it with the UI input control
        autocompleteArray[i] = new google.maps.places.Autocomplete(
            (document.getElementById(arguments[i])),
            {
                types: ['(cities)']
            });
    }


}

function onDepartureCityChanged() {
    var place = autocompleteArray[0].getPlace();
    console.log(place);
    if (place && place.geometry) {
        departureLat.val(place.geometry.location.lat());
        departureLng.val(place.geometry.location.lng());
        $('#div_id_departure_city').removeClass("has-error");


    } else {
        $('#id_departure_lat').val(91);
        $('#id_departure_lng').val(181);
        $('#div_id_departure_city').addClass("has-error");

    }
}

function onArrivalCityChanged() {
    var place = autocompleteArray[1].getPlace();
    console.log(place);
    if (place && place.geometry) {
        arrivalLat.val(place.geometry.location.lat());
        arrivalLng.val(place.geometry.location.lng());
        $('#div_id_arrival_city').removeClass("has-error");

    } else {
        $('#id_arrival_lat').val(91);
        $('#id_arrival_lng').val(181);
        $('#div_id_arrival_city').addClass("has-error");
    }
}

$('#id_departure_city').change(function() {
   console.log("departure changed");
   onDepartureCityChanged();

});

$('#id_arrival_city').change(function() {
   console.log("arrival changed");
    onArrivalCityChanged();
});
