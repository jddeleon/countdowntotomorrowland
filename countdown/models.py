from django.db import models

class Feedback(models.Model):
    FEEDBACK_TYPES = (
        ('Bug Report', 'Bug Report'),
        ('General Feedback', 'General Feedback'),
        ('Feature Request', 'Feature Request'),
    )

    username = models.CharField(max_length=255, blank=True)
    feedback_type = models.CharField(max_length=255, choices=FEEDBACK_TYPES, blank=False)
    feedback = models.TextField(blank=False)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __unicode__(self):
        return str(self.feedback_type)
