# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=255, blank=True)),
                ('feedback_type', models.CharField(max_length=255, choices=[(b'Bug Report', b'Bug Report'), (b'General Feedback', b'General Feedback'), (b'Feature Request', b'Feature Request')])),
                ('feedback', models.TextField()),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
