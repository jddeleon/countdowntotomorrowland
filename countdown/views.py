from django.shortcuts import render, redirect
from django.contrib.auth import logout as auth_logout
from countdown.models import Feedback
from countdown.forms import FeedbackForm
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib import messages
import datetime

def logout(request):
    auth_logout(request)
    return redirect('map_detail')

def about(request):

    return render(request, 'countdown/about.html')

def feedback(request):

    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        print "POST"

        # validate form
        if form.is_valid():
            itinerary = form.save(commit=False)
            itinerary.created = datetime.datetime.today()
            itinerary.save()
            messages.success(request, "Thanks for submitting your feedback")

        else:
            print form.errors
    else:
        # display form
        form = FeedbackForm()
        print "GET"
    return render(request, 'countdown/feedback.html', {'form': form})