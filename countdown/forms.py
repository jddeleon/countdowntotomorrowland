from django import forms
from countdown.models import Feedback
from django.forms import widgets
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, ButtonHolder, Field
from crispy_forms.bootstrap import InlineField, InlineCheckboxes, FormActions

class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ['username',
                  'feedback_type',
                  'feedback'
        ]
        exclude = ['created']

    def __init__(self, *args, **kwargs):
        super(FeedbackForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(

            Fieldset(
                'Bug Report/Feature Request',
                'username',
                'feedback_type',
                Field('feedback', placeholder='If reporting a bug, please be as detailed as possible '
                                          'about how to recreate the problem.')
            ),


            FormActions(
                Submit('save', 'Submit Feedback', css_class='btn-tomorrowland')
            )


        )
